const wrapper = document.querySelector('.wrapper')
const listTodo = document.querySelector(".todo__list")
const inputNewTodo = document.querySelector(".todo__input")
const btnCreatePost = document.querySelector(".btn__add")
const taskFilterContainer = document.querySelector(".todo__container-list")
// ============
const modalEditPost = document.querySelector(".modal")

let todos = []

let todosProxy = new Proxy(todos, {
  set: function (target, property, value) {
    // Вызываем getFilterTodo при каждом изменении в массиве todos
    getFilterTodo(todosProxy)
    return Reflect.set(...arguments)
  }
})


getFetchTodo()

// событие клавиатуры на кнопке add
document.addEventListener("keyup", function (e) {

  if (e.code === 'Enter') {
    btnCreatePost.click()
    inputNewTodo.value = ''
  }
})

document.addEventListener("keyup", function (e) {

  if (e.code === 'Escape' && modalEditPost.classList.contains('modal_active')) {
    closeModal()
  }
})

// добавление объекта todo
btnCreatePost.addEventListener('click', () => {
  const value = inputNewTodo.value
  const newTodo = {
    userId: Date.now(),
    id: Date.now(),
    title: value,
    completed: false,
  }

  // очистка input
  if (newTodo.title) {
    todosProxy.unshift(newTodo)
    inputNewTodo.value = ''
  }
})

// удаление todo
function handleDeleteClick(todoId, e) {
  todosProxy.splice(todosProxy.findIndex(todo => todo.id === todoId), 1)
}

// отрисовка данных на странице из запроса на сервер
async function getFetchTodo() {
  const response = await fetch("https://jsonplaceholder.typicode.com/todos/1")
  const json = await response.json()
  todosProxy.push(json)
};

// присвоение данных одному элементу todo
function getFilterTodo(todos) {
  listTodo.innerHTML = ''

  todos.forEach(todo => listTodo.innerHTML += createTodo(todo))
  assignTaskList(todos)
  getFilterSuccessTasks(todos)
  getFilterProcessTasks(todos)
}

// отрисовка шаблона одного todo при добавлении
function createTodo(todo) {
  const todoClass = todo.completed ? 'todo-list__title active' : 'todo-list__title'
  return `
        <li onclick="toggleTodoDone(${todo.id})" class="todo-list__item">
            <p class="${todoClass}">${todo.title}</p>
            <div class="btn__container">
              <button class='btn' onclick="openModal(${todo.id}, event)">
                <img width='20px' src="./img/pen.svg" alt="редактировать">
              </button>
              <button class='btn' onclick="handleDeleteClick(${todo.id}, event)">
                <img width='20px' src="./img/trash.svg" alt="удалить">
              </button>
            </div>
        </li>
      `
}

// выполненное todo
function toggleTodoDone(id) {
  const todoId = todosProxy.findIndex(todo => todo.id === id)

  if (todoId !== -1) {
    const todo = todosProxy[todoId]
    todo.completed = !todo.completed
    todosProxy.splice(todoId, 1, todo)
  }
}

// присваивание шаблона к контейнеру dom
function assignTaskList(count) {
  taskFilterContainer.innerHTML =
    `
    <ul class="todo__tasks-list">
      <li class="todo__all-tasks">All: ${count.length}</li>
      <li class="todo__completed-tasks" > Success: ${getFilterSuccessTasks(count)}</li>
      <li class="todo__unfinished-tasks">Process: ${getFilterProcessTasks(count)}</li>
    </ul>
      
    `
}
function createModal(id, currentTodo) {


  modalEditPost.innerHTML =
    `
    <div class="modal__wrap">
      <div class="modal__inner" onclick="(function(e) { e.stopPropagation() })(event)">
  
        <h2 class="modal__title">
          Edit post
        </h2>
        <button class="modal__close" onclick="closeModal()">
          <img src="./img/close.svg" alt="закрыть">
        </button>
  
        <div class="modal__edit-post todo__create-task">
          <input class='todo__input-edit' type="text" value="${currentTodo.title}">
          <button class="btn btn__edit btn__add" onclick="updatePost(${id}, document.querySelector('.todo__input-edit').value)">
          Edit post
          </button>
        </div>
      </div>
    </div>
    `

  document.querySelector('.modal__wrap').addEventListener('click', () => {
    closeModal()
  })
}

function getFilterSuccessTasks(todos) {
  return todos.filter(todo => todo.completed).length
}

function getFilterProcessTasks(todos) {
  return todos.filter(todo => !todo.completed).length
}

// закрыть модальное окно

function closeModal() {
  modalEditPost.classList.remove('modal_active')
}


// открыть модальное окно
function openModal(id, e) {
  e.stopPropagation()
  modalEditPost.classList.add('modal_active')

  createModal(id, todos.find(todo => todo.id === id))
}


function updatePost(todoModalId, val) {
  const todoId = todosProxy.findIndex(todo => todo.id === todoModalId)

  if (todoId || todoId === 0) {
    const todo = todosProxy[todoId]
    todo.title = val
    todosProxy.splice(todoId, 1, todo)
  }

  closeModal()
}
